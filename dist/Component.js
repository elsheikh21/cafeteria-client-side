sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"cafeteria/model/models",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/resource/ResourceModel"
], function(UIComponent, Device, models, JSONModel, ResourceModel) {
	"use strict";

	return UIComponent.extend("cafeteria.Component", {

		metadata: {
			manifest: "json"
		},
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			
			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			// intialize the router 
			this.getRouter().initialize();
		}
	});
});

/**
 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
 * @public
 * @override
 */