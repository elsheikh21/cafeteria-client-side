sap.ui.define(["sap/ui/core/mvc/Controller", "sap/m/MessageToast"], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("cafeteria.controller.CaptainEmployeeDetails", {
		onCreateTap: function() {
			var EmployeeID = this.getView().byId("IDEmployee").getText().toString().trim(); // get employee ID
			var EmployeePoints = this.getView().byId("PointsEmployee").getText().toString().trim(); // get employee Points
			var EmployeeObject = {};
			EmployeeObject.eid = EmployeeID;
			EmployeeObject.ep = EmployeePoints;
			sap.ui.getCore().setModel(EmployeeObject, "EmployeeModel");
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("createOrder");
		},
		onLogOutTap: function() {
			var http = new XMLHttpRequest();
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var url = oResourceBundle.getText("LogOut").toString();
			http.onreadystatechange = function() {
				if (http.readyState === 4 && http.status === 200) {
					var json = JSON.parse(http.responseText);
					var status = json.status.toString();
					switch (status) {
						case "-100":
							var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
							oRouter.navTo("loginPage");
							break;
						default:
							sap.m.MessageToast.show("An unexpected error occured.");
					}
				}
			};
			http.open("POST", url, false);
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.send();
		},
		onCancelTap: function() {
			// cancels the orders for the user
			// upon successful canceling, show a message
			sap.m.MessageToast("This employee's order is cancelled.");
			// if not, show a message
			sap.MessageToast("An error encountered during order canceling");
		},
		onExtraTap: function() {
				var EmployeeID = this.getView().byId("IDEmployee").getText().toString().trim(); // get employee ID
				var employeePts = this.getView().byId("PointsEmployee").getText().toString().trim(); // get employee Points
				var EmployeePoints = parseInt(employeePts, 10);
				var http = new XMLHttpRequest();
				var params = "employeeID=" + EmployeeID + "&employeePoints=" + employeePts;
				var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
				var url = oResourceBundle.getText("ExtraPoints").toString();
				http.onreadystatechange = function() {
					if (http.readyState === 4 && http.status === 200) {
						var json = JSON.parse(http.responseText);
						var points = json.points.toString();
						var pointsInt = parseInt(points, 10);
						switch (points) {
							case "-100":
								var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
								oRouter.navTo("loginPage");
								break;
							case (pointsInt <= 0 && pointsInt > -100):
								MessageToast.show("You can not add points to this employee.");
								break;
							default:
								EmployeePoints += pointsInt;
								MessageToast.show("You added points to this employee.");
						}
					}
				};
				http.open("POST", url, false);
				http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http.send(params);
				// refreshes the extra points for this employee
				// upon successful refreshing, if there is extra points
				sap.m.MessageToast("Balance is updated.");
				// if there is not 
				sap.m.MessageToast("There is none");
			}
			// ,
			// onRefreshTap: function() {
			// 	// refrehes the attendance of this employee
			// 	// upon successful refreshing, if there is extra points
			// 	sap.m.MessageToast("Please check the employee's attendance");
			// }
	});
});