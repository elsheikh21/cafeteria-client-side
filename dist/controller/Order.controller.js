sap.ui.define(["jquery.sap.global", "sap/m/MessageToast", "sap/m/MessageBox", "sap/ui/core/mvc/Controller"], function(jQuery,
	MessageToast, MessageBox, Controller) {
	"use strict";

	return Controller.extend("cafeteria.controller.Order", {
		HandleLiveChange: function(oEvent) {
			var oInputControl = oEvent.getSource();
			var reg = new RegExp("[^0-9]", "g");
			var ID = this.getView().byId("ToSearchFor").getValue().toString().trim();
			if (ID.length > 0 && !ID.match(reg)) {
				this.getView().byId("ToSearchFor").setValueStateText("Valid entry.");
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("ToSearchFor").setValueStateText("Invalid entry.");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		OnSearchTap: function() {
			var reg = new RegExp("[^0-9]");
			var ID = this.getView().byId("ToSearchFor").getValue().toString().trim();
			if (ID.length > 0 && !ID.match(reg)) {
				this.getView().byId("ToSearchFor").setValueStateText("Valid entry.");

				var selectedIndex = this.getView().byId("Selected").getSelectedIndex().toString().trim();
				var http = new XMLHttpRequest();
				var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
				var url = oResourceBundle.getText("OrderSearch").toString();
				var params = "";

				if (selectedIndex === "0") {
					params = "EID=" + ID;

				} else if (selectedIndex === "1") {
					params = "OID=" + ID;
				}

				http.onreadystatechange = function() {
					if (http.readyState === 4 && http.status === 200) {
						// var json = JSON.parse(http.responseText);
						// parsing of this json object will be different
					}
				};

				http.open("POST", url, false);
				http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http.send(params);
			} else {
				this.getView().byId("ToSearchFor").setValueStateText("Invalid entry.");
			}
		}
	});

});