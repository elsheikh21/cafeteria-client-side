sap.ui.define([
	"sap/ui/core/mvc/Controller", "sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("cafeteria.controller.UserReset", {
		handleUserInputNPW: function(oEvent) {
			var pattern = new RegExp("^[a-zA-Z0-9]*$");
			var newpw = this.getView().byId("newpw").getValue().toString();
			var oInputControl = oEvent.getSource();
			if (newpw.length >= 15 && pattern.test(newpw)) {
				this.getView().byId("newpw").setValueStateText("This is a strong password.");
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("newpw").setValueStateText(
					"Password should be 15 characters long at least, and must contain upper case & lower case & numbers.");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		handleUserInputCNPW: function(oEvent) {
			var newpw = this.getView().byId("newpw").getValue().toString();
			var cnewpw = this.getView().byId("cnewpw").getValue().toString();
			var oInputControl = oEvent.getSource();
			if (cnewpw.length > 0 && newpw.localeCompare(cnewpw) === 0) { //sUserInput
				this.getView().byId("newpw").setValueStateText("Passwords match.");
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("cnewpw").setValueStateText("Passwords must match.");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onChangeTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var token = this.getView().byId("token").getValue().toString().trim();
			var newpw = this.getView().byId("newpw").getValue().toString().trim();
			var cnewpw = this.getView().byId("cnewpw").getValue().toString().trim();
			var pattern = new RegExp("^[a-zA-Z0-9]*$");
			if (token.length > 0 && newpw.length > 0 && cnewpw.length > 0 && newpw.length >= 15 && pattern.test(newpw) && newpw.localeCompare(
					cnewpw) === 0) {
				var http = new XMLHttpRequest();
				var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
				var url = oResourceBundle.getText("UserResetPassword").toString();
				var params = "token=" + token + "&newPassword=" + newpw + "&ConfirmNewPassword=" + cnewpw;
				http.onreadystatechange = function() {
					if (http.readyState === 4 && http.status === 200) {
						var json = JSON.parse(http.responseText);
						switch (json.status.toString()) {
							case "-88":
								MessageToast.show("Reset failed.");
								break;
							case "88":
								MessageToast.show("Successful");
								oRouter.navTo("loginPage");
								break;
							default:
								MessageToast.show("Token is invalid.");
						}
					}
				};
				http.open("POST", url, false);
				http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http.send(params);
			}
		},
		onLoginTap: function() { // returning back to login page
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("loginPage");
		}
	});
});

/**
 * Called when a controller is instantiated and its View controls (if available) are already created.
 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
 * @memberOf cafeteria.view.UserReset
 */
//	onInit: function() {
//
//	},

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
 * (NOT before the first rendering! onInit() is used for that one!).
 * @memberOf cafeteria.view.UserReset
 */
//	onBeforeRendering: function() {
//
//	},

/**
 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
 * This hook is the same one that SAPUI5 controls get after being rendered.
 * @memberOf cafeteria.view.UserReset
 */
//	onAfterRendering: function() {
//
//	},

/**
 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
 * @memberOf cafeteria.view.UserReset
 */
//	onExit: function() {
//
//	}