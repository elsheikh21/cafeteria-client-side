sap.ui.define(["sap/ui/core/mvc/Controller", "sap/m/MessageToast"], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("cafeteria.controller.admin", {
		onNavBack: function(){
			
		},
		onDefaultTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Defaults");
		},
		onRegisterTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("adminCreate");
		},
		onResetTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("adminReset");
		},
		onDeleteTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("adminDelete");
		},
		onChangeTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("adminChange");
		},
		onEmployeesUploadTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("EmployeeFileUpload");
		},
		onItemsUploadTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("FileUpload");
		},
		onLogOutTap: function() {
			var http = new XMLHttpRequest();
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var url = oResourceBundle.getText("LogOut").toString();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			http.onreadystatechange = function() {
				if (http.readyState === 4 && http.status === 200) {
					var json = JSON.parse(http.responseText);
					var status = json.status.toString();
					switch (status) {
						case "-100":
							oRouter.navTo("loginPage");
							MessageToast.show("You are logged out.");
							break;
						default:
							sap.m.MessageToast.show("An unexpected error occured.");
					}
				}
			};
			http.open("POST", url, false);
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.send();
		}
	});
});