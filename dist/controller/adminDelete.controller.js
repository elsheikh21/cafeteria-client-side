sap.ui.define(["sap/ui/core/mvc/Controller", "sap/m/MessageToast", "sap/m/MessageBox"], function(Controller, MessageToast, MessageBox) {
	"use strict";

	return Controller.extend("cafeteria.controller.adminDelete", {
		onDeleteTap: function() {
			var email = this.getView().byId("email").getValue().toString().trim();
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pattern.test(email)) {
				var http = new XMLHttpRequest();
				var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
				var url = oResourceBundle.getText("DeleteUser").toString();
				var params = "email=" + email;
				http.onreadystatechange = function() {
					if (http.readyState === 4 && http.status === 200) {
						var json = JSON.parse(http.responseText);
						switch (json.status.toString()) {
							case "3": // Cashier's page
								this.getView().byId("email").setValue("");
								MessageToast.show("user deletion successful.");
								break;
							case "-100":
								MessageToast.show("user registeration failed.");
								var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
								oRouter.navTo("loginPage");
								break;
							default:
								MessageToast.show("user deletion failed.");
						}
					}
				};
				http.open("POST", url, false);
				http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http.send(params);
			} else {
				MessageBox.show("Please enter a valid email address.");
			}
		},
		handleUserInputEmail: function(oEvent) {
			var email = this.getView().byId("email").getValue().toString();
			var oInputControl = oEvent.getSource();
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pattern.test(email)) {
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("email").setValueStateText("Please enter a valid email address. john@example.com");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onAdminTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("admin");
		}
	});

});

/**
 * Called when a controller is instantiated and its View controls (if available) are already created.
 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
 * @memberOf cafeteria.view.adminDelete
 */
//	onInit: function() {
//
//	},

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
 * (NOT before the first rendering! onInit() is used for that one!).
 * @memberOf cafeteria.view.adminDelete
 */
//	onBeforeRendering: function() {
//
//	},

/**
 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
 * This hook is the same one that SAPUI5 controls get after being rendered.
 * @memberOf cafeteria.view.adminDelete
 */
//	onAfterRendering: function() {
//
//	},

/**
 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
 * @memberOf cafeteria.view.adminDelete
 */
//	onExit: function() {
//
//	}