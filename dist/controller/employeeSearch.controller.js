sap.ui.define([
	"sap/ui/core/mvc/Controller", "sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("cafeteria.controller.employeeSearch", {

		onInit: function() {
			// set explored app's demo model on this sample
			// this.oModel = new JSONModel(jQuery.sap.getModulePath("sap.ui.demo.mock", "/products.json"));
			var oView = this.getView();
			// oView.setModel(this.oModel);
			this.oSF = oView.byId("searchField");
		},
		onSearch: function(event) {
			var item = event.getParameter("suggestionItem");
			if (item) {
				sap.m.MessageToast.show("search for: " + item.getText());
			}
		},
		onSuggest: function(event) {
			var value = event.getParameter("suggestValue");
			var filters = [];
			if (value) {
				filters = [
					new sap.ui.model.Filter([
						new sap.ui.model.Filter("ProductId", function(sText) {
							return (sText || "").toUpperCase().indexOf(value.toUpperCase()) > -1;
						}),
						new sap.ui.model.Filter("Name", function(sDes) {
							return (sDes || "").toUpperCase().indexOf(value.toUpperCase()) > -1;
						})
					], false)
				];
			}

			this.oSF.getBinding("suggestionItems").filter(filters);
			this.oSF.suggest();
		}
	});

});
// return Controller.extend("cafeteria.controller.employeeSearch", {
// 	handleSearch: function(evt) {
// 		// create model filter
// 		var filters = [];
// 		var query = evt.getParameter("query");
// 		if (query && query.length > 0) {
// 			var filter = new sap.ui.model.Filter("ProductName", sap.ui.model.FilterOperator.Contains, query);
// 			filters.push(filter);
// 		}

// 		// update list binding
// 		var list = this.getView().byId("List");
// 		var binding = list.getBinding("items");
// 		binding.filter(filters);
// 	}

/**
 * Called when a controller is instantiated and its View controls (if available) are already created.
 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
 * @memberOf cafeteria.view.employeeSearch
 */
//	onInit: function() {
//
//	},

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
 * (NOT before the first rendering! onInit() is used for that one!).
 * @memberOf cafeteria.view.employeeSearch
 */
//	onBeforeRendering: function() {
//
//	},

/**
 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
 * This hook is the same one that SAPUI5 controls get after being rendered.
 * @memberOf cafeteria.view.employeeSearch
 */
//	onAfterRendering: function() {
//
//	},

/**
 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
 * @memberOf cafeteria.view.employeeSearch
 */
//	onExit: function() {
//
//	}