sap.ui.define([
	"sap/ui/core/mvc/Controller", "sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";
	return Controller.extend("cafeteria.controller.loginPage", {
		onLoginTap: function() {
			var email = this.getView().byId("email").getValue().toString().trim();
			var pw = this.getView().byId("pw").getValue().toString().trim();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pw.length > 0 && pattern.test(email)) {
				var http = new XMLHttpRequest();
				var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
				var url = oResourceBundle.getText("Login").toString();
				var params = "email=" + email + "&password=" + pw;
				http.onreadystatechange = function() {
					if (http.readyState === 4 && http.status === 200) {
						var json = JSON.parse(http.responseText);
						var status = json.status.toString();
						switch (status) {
							case "-1":
								MessageToast.show("It is your first sign in, you have to reset your password.");
								oRouter.navTo("resetPassword");
								break;
							case "0":
								MessageToast.show("Welcome back");
								oRouter.navTo("CaptainEmployeeContainer");
								break;
							case "1":
								MessageToast.show("Welcome back");
								oRouter.navTo("admin");
								break;
							case "2":
								MessageToast.show("Welcome back");
								oRouter.navTo("employeeContainer");
								break;
							default:
								MessageToast.show("Login failed.");
						}
					}
				};
				http.open("POST", url, true);
				http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http.send(params);
			} else {
				MessageToast.show("Please enter email & password");
			}
		},
		handleUserInput: function(oEvent) {
			var email = this.getView().byId("email").getValue().toString(); // to be sent to backend
			var oInputControl = oEvent.getSource();
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pattern.test(email)) {
				this.getView().byId("email").setValueStateText("it is a valid email address.");
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("email").setValueStateText("Please enter a valid email address. john@example.com");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onResetTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("resetPassword");
		},
		onChangeTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("changePassword");
		},
		TEST: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("FileUpload");
		}
	});
});