sap.ui.define(["sap/ui/core/mvc/Controller", "sap/m/MessageToast", "sap/ui/core/routing/History"], function(Controller, MessageToast,
	History) {
	"use strict";

	return Controller.extend("cafeteria.controller.Defaults", {
		onNavBack: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("admin", true);
			}
		},
		handleUserInput: function(oEvent) {
			var defaultPoints = this.getView().byId("defaultPoints").getValue().toString(); // to be sent to backend
			var oInputControl = oEvent.getSource();
			var pattern = /^[0-9]*$/;
			if (defaultPoints.length > 0 && pattern.test(defaultPoints)) {
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("defaultPoints").setValueStateText("Only numbers are allowed");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onDefaultTap: function() {
			var http = new XMLHttpRequest();
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var url = oResourceBundle.getText("DefaultPoints").toString();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			http.onreadystatechange = function() {
				if (http.readyState === 4 && http.status === 200) {
					var json = JSON.parse(http.responseText);
					var status = json.status.toString();
					switch (status) {
						case "-100":
							oRouter.navTo("loginPage");
							MessageToast.show("You are logged out.");
							break;
						default:
							sap.m.MessageToast.show(status);
					}
				}
			};
			http.open("POST", url, false);
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.send();
		},
		onExtraTap: function() {
			var http = new XMLHttpRequest();
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var url = oResourceBundle.getText("ExtraPoints").toString();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			http.onreadystatechange = function() {
				if (http.readyState === 4 && http.status === 200) {
					var json = JSON.parse(http.responseText);
					var status = json.status.toString();
					switch (status) {
						case "-100":
							oRouter.navTo("loginPage");
							MessageToast.show("You are logged out.");
							break;
						default:
							sap.m.MessageToast.show(status);
					}
				}
			};
			http.open("POST", url, false);
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.send();
		}
	});
});