sap.ui.define(["sap/ui/core/mvc/Controller", "sap/m/MessageToast"], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("cafeteria.controller.adminCreate", {
		onRegisterTap: function() {
			var email = this.getView().byId("email").getValue().toString().trim();
			var role = this.getView().byId("ComboBoxItems").getValue().toString().trim();
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pattern.test(email) && role.length > 0) {
				var http = new XMLHttpRequest();
				var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
				var url = oResourceBundle.getText("RegisterURL").toString();
				var params = "email=" + email + "&role=" + role;
				http.onreadystatechange = function() {
					if (http.readyState === 4 && http.status === 200) {
						var json = JSON.parse(http.responseText);
						var status = json.status.toString();
						switch (status) {
							case "6": // Cashier's page
								MessageToast.show("user registeration successful.");
								break;
							case "-100":
								MessageToast.show("user registeration failed.");
								var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
								oRouter.navTo("loginPage");
								break;
							default:
								MessageToast.show("user registeration failed.");
						}
					}
				};
				http.open("POST", url, false);
				http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http.send(params);
			} else {
				sap.m.MessageToast.show("Please enter a valid email address.");
			}
		},
		handleUserInputEmail: function(oEvent) {
			var email = this.getView().byId("email").getValue().toString();
			var oInputControl = oEvent.getSource();
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pattern.test(email)) {
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("email").setValueStateText("Please enter a valid email address. john@example.com");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onAdminTap: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("admin");
		}
	});
});