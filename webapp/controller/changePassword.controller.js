sap.ui.define([
	"sap/ui/core/mvc/Controller", "sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict"; //helps to detect potential coding issues at an early state at development time

	return Controller.extend("cafeteria.controller.changePassword", {
		onLoginTap: function() {
			// navigate back to login page
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("loginPage");
		},
		handleUserInputUID: function(oEvent) {
			var email = this.getView().byId("email").getValue().toString(); // to be sent to backend
			// var pw = this.getView().byId("pw").getValue().toString();
			// var newpw = this.getView().byId("newpw").getValue().toString();
			// var cnewpw = this.getView().byId("cnewpw").getValue().toString();
			// var sUserInput = oEvent.getParameter("value");
			// && pw.length > 0 && newpw.length >= 8 && /^[a-zA-Z0-9-$@$!%*#?& ]*$/.test(newpw) && newpw.localeCompare(cnewpw) === 0 && cnewpw.length > 0
			var oInputControl = oEvent.getSource();
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pattern.test(email)) { //sUserInput
				this.getView().byId("email").setValueStateText("Valid email address.");
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("email").setValueStateText("Please enter a valid email address. john@example.com");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		handleUserInputPW: function(oEvent) {
			var pw = this.getView().byId("pw").getValue().toString();
			var oInputControl = oEvent.getSource();
			if (pw.length > 0) {
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		handleUserInputNPW: function(oEvent) {
			// var pattern = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g");
			var pattern = new RegExp("^[a-zA-Z0-9]*$");
			// var enoughRegex = new RegExp("(?=.{6,}).*", "g");
			// var str = "The best things in life are free";
			// var patt = new RegExp("e");
			// var res = patt.test(newpw);
			var newpw = this.getView().byId("newpw").getValue().toString();
			var oInputControl = oEvent.getSource();
			if (newpw.length >= 15 && pattern.test(newpw)) {
				this.getView().byId("newpw").setValueStateText("This is a strong password.");
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("newpw").setValueStateText(
					"Password should be 15 characters long at least, and must contain uppercase, lowercase, and a number.");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		handleUserInputCNPW: function(oEvent) {
			// var uid = this.getView().byId("uid").getValue().toString(); // to be sent to backend
			// var pw = this.getView().byId("pw").getValue().toString();
			var newpw = this.getView().byId("newpw").getValue().toString();
			var cnewpw = this.getView().byId("cnewpw").getValue().toString();
			// var sUserInput = oEvent.getParameter("value");
			// cnewpw.length > 0 && newpw.localeCompare(cnewpw) === 0
			var oInputControl = oEvent.getSource();
			if (cnewpw.length > 0 && newpw.localeCompare(cnewpw) === 0) { //sUserInput
				this.getView().byId("newpw").setValueStateText("Passwords match.");
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("cnewpw").setValueStateText("Passwords must match.");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onChangeTap: function() {
			var email = this.getView().byId("email").getValue().toString().trim();
			var pw = this.getView().byId("pw").getValue().toString().trim();
			var newpw = this.getView().byId("newpw").getValue().toString().trim();
			var cnewpw = this.getView().byId("cnewpw").getValue().toString().trim();
			if (pw.length === 0) { // checking current password if entered or not 
				MessageToast.show("Please enter your current password.");
			} else {
				// in case current password is sent, send it to the backend for checking its validity 
				if (newpw.length <= 15) { // checking the length of new password
					MessageToast.show("The password's length is very short, it should be 15 characters long at least.");
				} else {
					// if length of new password is above 8, check its strength
					if (!(/^[a-zA-Z0-9-$@$!%*#?& ]*$/.test(newpw))) {
						MessageToast.show(
							"Your password is very weak, it must contain at least an Upper case letter, lower case letter, and numbers.");
					} else {
						// if new password is strong enough compare with the confirmation entry of it 
						if (newpw.toString().localeCompare(cnewpw.toString()) !== 0) { // localecompare between strings return 0 for exact matches.
							MessageToast.show("The passwords do not match!");
						} else {
							var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
							var http = new XMLHttpRequest();
							var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
							var url = oResourceBundle.getText("ChangePassword").toString();
							var params = "email=" + email + "&password=" + pw + "&newpassword=" + newpw + "&confirmpassword=" + cnewpw;
							http.onreadystatechange = function() {
								if (http.readyState === 4 && http.status === 200) {
									var json = JSON.parse(http.responseText);
									switch (json.status.toString()) {
										case "4":
											MessageToast.show("Your password is updated");
											oRouter.navTo("loginPage");
											break;
										case "-100":
											MessageToast.show("user registeration failed.");
											oRouter.navTo("loginPage");
											break;
										default:
											MessageToast.show("Sadly, we were not able to update your password.");
									}
								}
							};
							http.open("POST", url, false);
							http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							http.send(params);
						}
					}
				}
			}
		}
	});
});
/**
 * Called when a controller is instantiated and its View controls (if available) are already created.
 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
 * @memberOf cafeteria.view.changePassword
 */
//	onInit: function() {
//
//	},

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
 * (NOT before the first rendering! onInit() is used for that one!).
 * @memberOf cafeteria.view.changePassword
 */
//	onBeforeRendering: function() {
//
//	},

/**
 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
 * This hook is the same one that SAPUI5 controls get after being rendered.
 * @memberOf cafeteria.view.changePassword
 */
//	onAfterRendering: function() {
//
//	},

/**
 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
 * @memberOf cafeteria.view.changePassword
 */
//	onExit: function() {
//
//	}