sap.ui.define(["sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/ui/core/mvc/Controller", "sap/ui/model/json/JSONModel"
], function(MessageToast, MessageBox, Controller, JSONModel) {
	"use strict";
	// TODO:  1. Balance binding and updating 
	return Controller.extend("cafeteria.controller.createOrder", {
		onPrintTap: function() {
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var EmployeeObject = sap.ui.getCore().getModel("EmployeeModel");
			var Eid = EmployeeObject.eid;
			// MessageBox.show(Eid);
			var proteins = this.getView().byId("pro").getText();
			var fats = this.getView().byId("fats").getText();
			var carbs = this.getView().byId("carbs").getText();
			var salads = this.getView().byId("salads").getText();
			var desserts = this.getView().byId("des").getText();
			var beverages = this.getView().byId("bev").getText();
			var params = "EmployeeID=" + Eid;
			if (proteins === "0" && fats === "0" && carbs === "0" && salads === "0" && desserts === "0" && beverages === "0") {
				sap.m.MessageToast.show("Please choose any of our meals.");
			} else {
				var print = "You chose the following: \n\t\r";
				if (proteins !== "0") {
					print += proteins + " protein portion(s) \n\t\r";
				}
				if (fats !== "0") {
					print += fats + " fats portion(s) \n\t\r";
				}
				if (carbs !== "0") {
					print += carbs + " carbs portion(s) \n\t\r";
				}
				if (salads !== "0") {
					print += salads + " salad portion(s) \n\t\r";
				}
				if (desserts !== "0") {
					print += desserts + " dessert portion(s) \n\t\r";
				}
				if (beverages !== "0") {
					print += beverages + " beverage(s) \n\t\r";
				}
				params += "&proteins=" + proteins + "&fats=" + fats + "&carbs=" + carbs + "&salads=" + salads + "&dessert=" + desserts +
					"&beverages=" + beverages;
				sap.m.MessageBox.show(print.toString(), {
					icon: sap.m.MessageBox.Icon.INFORMATION,
					title: "Print?",
					actions: [sap.m.MessageBox.Action.YES, sap.m.MessageBox.Action.NO],
					onClose: function(oAction) {
						if (oAction === "YES") {
							var http = new XMLHttpRequest();
							var url = oResourceBundle.getText("OrderPrint").toString();
							// var url = "https://cafeteriap1942851539trial.hanatrial.ondemand.com/Cafeteria/print";
							http.onreadystatechange = function() {
								if (http.readyState === 4 && http.status === 200) {
									var json = JSON.parse(http.responseText);
									var status = json.status.toString();
									switch (status) {
										case "21":
											MessageToast.show("The receipt is printed");
											break;
										case "-21":
											MessageToast.show("Printing failed.");
											break;
										default:
											MessageToast.show("An unexpected error occured.");
									}
								}
							};
							http.open("POST", url, false);
							http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							http.send(params);
						}
					}
				});
			}
		},
		onSaveTap: function() {
			var EmployeeObject = sap.ui.getCore().getModel("EmployeeModel");
			var Eid = EmployeeObject.eid;
			var http = new XMLHttpRequest();
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var url = oResourceBundle.getText("OrderSave").toString();
			var proteins = this.getView().byId("pro").getText().toString().trim();
			var fats = this.getView().byId("fats").getText().toString().trim();
			var carbs = this.getView().byId("carbs").getText().toString().trim();
			var salads = this.getView().byId("salads").getText().toString().trim();
			var desserts = this.getView().byId("des").getText().toString().trim();
			var beverages = this.getView().byId("bev").getText().toString().trim();
			var params = "EmployeeID=" + Eid;
			if (proteins === "0" && fats === "0" && carbs === "0" && salads === "0" && desserts === "0" && beverages === "0") {
				sap.m.MessageToast.show("Please choose any of our meals.");
			} else {
				if (proteins !== "0") {
					params = "&proteins=" + proteins;
				}
				if (fats !== "0") {
					params += "&fats=" + fats;
				}
				if (carbs !== "0") {
					params += "&carbs=" + carbs;
				}
				if (salads !== "0") {
					params += "&salads=" + salads;
				}
				if (desserts !== "0") {
					params += "&dessert=" + desserts;
				}
				if (beverages !== "0") {
					params += "&beverages=" + beverages;
				}
			}
			http.onreadystatechange = function() {
				if (http.readyState === 4 && http.status === 200) {
					var json = JSON.parse(http.responseText);
					var status = json.status.toString();
					switch (status) {
						case "15":
							MessageToast.show("we saved your order.");
							break;
						case "-15":
							MessageToast.show("sadly, we were not able to save your order.");
							break;
						default:
							MessageToast.show("An unexpected occured.");
					}
				}
			};

			http.open("POST", url, false);
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.send(params);
		},
		onCancelTap: function() {
			// clearing the whole list 
			this.getView().byId("pro").setText("0");
			this.getView().byId("fats").setText("0");
			this.getView().byId("carbs").setText("0");
			this.getView().byId("salads").setText("0");
			this.getView().byId("des").setText("0");
			this.getView().byId("bev").setText("0");
			sap.m.MessageToast.show("Your meals are deleted.");
		},
		onBackTap: function() {
			// navigate to employee's page
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("employeeContainer");
		},
		onDecPRO: function() {
			var pro = this.getView().byId("pro").getText().toString().trim();
			if (pro === "0") {
				sap.m.MessageToast.show("You did not add any proteins to your meal.");
			} else {
				pro--;
			}
			this.getView().byId("pro").setText(pro.toString().trim());
		},
		onIncPRO: function() {
			var pro = this.getView().byId("pro").getText();
			pro++;
			this.getView().byId("pro").setText(pro.toString().trim());
		},
		onDecFAT: function() {
			var fats = this.getView().byId("fats").getText();
			if (fats === "0") {
				sap.m.MessageToast.show("You did not add any fats to your meal.");
			} else {
				fats--;
			}
			this.getView().byId("fats").setText(fats.toString().trim());
		},
		onIncFAT: function() {
			var fats = this.getView().byId("fats").getText();
			fats++;
			this.getView().byId("fats").setText(fats.toString().trim());
		},
		onDecCAR: function() {
			var carbs = this.getView().byId("carbs").getText();
			if (carbs === "0") {
				sap.m.MessageToast.show("You did not add any carbs to your meal.");
			} else {
				carbs--;
			}
			this.getView().byId("carbs").setText(carbs.toString().trim());
		},
		onIncCAR: function() {
			var carbs = this.getView().byId("carbs").getText();
			carbs++;
			this.getView().byId("carbs").setText(carbs.toString().trim());
		},
		onDecSAL: function() {
			var salads = this.getView().byId("salads").getText();
			if (salads === "0") {
				sap.m.MessageToast.show("You did not add any salads to your meal.");
			} else {
				salads--;
			}
			this.getView().byId("salads").setText(salads.toString().trim());
		},
		onIncSAL: function() {
			var salads = this.getView().byId("salads").getText();
			salads++;
			this.getView().byId("salads").setText(salads.toString().trim());
		},
		onDecDES: function() {
			var des = this.getView().byId("des").getText();
			if (des === "0") {
				sap.m.MessageToast.show("You did not add any desserts to your meal.");
			} else {
				des--;
			}
			this.getView().byId("des").setText(des.toString().trim());
		},
		onIncDES: function() {
			var des = this.getView().byId("des").getText();
			des++;
			this.getView().byId("des").setText(des.toString().trim());
		},
		onDecBEV: function() {
			var bev = this.getView().byId("bev").getText();
			if (bev === "0") {
				sap.m.MessageToast.show("You did not add any beverages to your meal.");
			} else {
				bev--;
			}
			this.getView().byId("bev").setText(bev.toString().trim());
		},
		onIncBEV: function() {
			var bev = this.getView().byId("bev").getText();
			bev++;
			this.getView().byId("bev").setText(bev.toString().trim());
		}

	});
});

// onInit: function() {
// 	var today = new Date();
// 	var dd = today.getDate();
// 	var mm = today.getMonth() + 1;
// 	var yyyy = today.getFullYear();
// 	if (dd < 10) {
// 		dd = 0 + dd;
// 	}
// 	if (mm < 10) {
// 		mm = 0 + mm;
// 	}

// 	var hh = today.getHours();
// 	var m = today.getMinutes();
// 	if (hh < 0) {
// 		hh = hh + 0;
// 	}
// 	if (m < 0) {
// 		m = m + 0;
// 	}
// 	today = dd + "/" + mm + "/" + yyyy + " " + hh + ":" + m + ".";
// 	this.getView().byId("time").setText(today.toString());
// },
/**
 * Called when a controller is instantiated and its View controls (if available) are already created.
 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
 * @memberOf cafeteria.view.createOrder
 */
//	onInit: function() {
//
//	},

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
 * (NOT before the first rendering! onInit() is used for that one!).
 * @memberOf cafeteria.view.createOrder
 */
//	onBeforeRendering: function() {
//
//	},

/**
 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
 * This hook is the same one that SAPUI5 controls get after being rendered.
 * @memberOf cafeteria.view.createOrder
 */
//	onAfterRendering: function() {
//
//	},

/**
 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
 * @memberOf cafeteria.view.createOrder
 */
//	onExit: function() {
//
//	}