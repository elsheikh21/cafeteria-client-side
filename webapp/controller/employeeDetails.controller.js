sap.ui.define(["jquery.sap.global", "sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel", "sap/m/MessageBox", "sap.m.MessageToast"
], function(jQuery, Controller, JSONModel, MessageBox, MessageToast) {
	"use strict";
	return Controller.extend("cafeteria.controller.employeeDetails", {
		onCreateTap: function() {
			var EmployeeID = this.getView().byId("IDEmployee").getText().toString().trim(); // get employee ID
			var EmployeePoints = this.getView().byId("PointsEmployee").getText().toString().trim(); // get employee Points
			var EmployeeObject = {};
			EmployeeObject.eid = EmployeeID;
			EmployeeObject.ep = EmployeePoints;
			sap.ui.getCore().setModel(EmployeeObject, "EmployeeModel");
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("createOrder");
		},
		onLogOutTap: function() {
			var http = new XMLHttpRequest();
			var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
			var url = oResourceBundle.getText("LogOut").toString();
			http.onreadystatechange = function() {
				if (http.readyState === 4 && http.status === 200) {
					var json = JSON.parse(http.responseText);
					var status = json.status.toString();
					switch (status) {
						case "-100":
							var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
							oRouter.navTo("loginPage");
							break;
						default:
							MessageToast.show("An unexpected error occured.");
					}
				}
			};
			http.open("POST", url, false);
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.send();
		},
		onCancelTap: function() {
				// cancels the orders for the user
				// upon successful canceling, show a message
				MessageToast("This employee's order is cancelled.");
				// if not, show a message
				MessageToast("An error encountered during order canceling");
			}
			// ,
			// onExtraTap: function() {
			// 	// refreshes the extra points for this employee
			// 	// upon successful refreshing, if there is extra points
			// 	sap.m.MessageToast("Balance is updated.");
			// 	// if there is not 
			// 	sap.m.MessageToast("There is none");
			// },
			// onRefreshTap: function() {
			// 	// refrehes the attendance of this employee
			// 	// upon successful refreshing, if there is extra points
			// 	sap.m.MessageToast("Please check the employee's attendance");
			// }
	});
});
/**
 * Called when a controller is instantiated and its View controls (if available) are already created.
 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
 * @memberOf cafeteria.view.employeeDetails
 */
//	onInit: function() {
//
//	},

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
 * (NOT before the first rendering! onInit() is used for that one!).
 * @memberOf cafeteria.view.employeeDetails
 */
//	onBeforeRendering: function() {
//
//	},

/**
 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
 * This hook is the same one that SAPUI5 controls get after being rendered.
 * @memberOf cafeteria.view.employeeDetails
 */
//	onAfterRendering: function() {
//
//	},

/**
 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
 * @memberOf cafeteria.view.employeeDetails
 */
//	onExit: function() {
//
//	}