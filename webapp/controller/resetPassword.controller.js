sap.ui.define([
	"sap/ui/core/mvc/Controller", "sap/m/MessageBox"
], function(Controller, MessageBox) {
	"use strict"; //helps to detect potential coding issues at an early state at development time

	return Controller.extend("cafeteria.controller.resetPassword", {
		handleUserInput: function(oEvent) {
			var email = this.getView().byId("email").getValue().toString(); // to be sent to backend
			var oInputControl = oEvent.getSource();
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pattern.test(email)) {
				this.getView().byId("email").setValueStateText("Valid email address.");
				oInputControl.setValueState(sap.ui.core.ValueState.Success);
			} else {
				this.getView().byId("email").setValueStateText("Please enter a valid email address. john@example.com");
				oInputControl.setValueState(sap.ui.core.ValueState.Error);
			}
		},
		onResetTap: function() {
			var email = this.getView().byId("email").getValue().toString();
			var pattern =
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (email.length > 0 && pattern.test(email)) {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				var http = new XMLHttpRequest();
				var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
				var url = oResourceBundle.getText("UserResetPassword").toString();
				var params = "email=" + email;
				http.onreadystatechange = function() {
					if (http.readyState === 4 && http.status === 200) {
						var json = JSON.parse(http.responseText);
						switch (json.status.toString()) {
							case "-88": // Reset password
								MessageBox.show("Token is not there");
								break;
							case "-89":
								MessageBox.show("Invalid token");
								break;
							case "-90":
								MessageBox.show("email is not sent");
								break;
							case "88":
								MessageBox.show("Email sent.");
								oRouter.navTo("UserReset");
								break;
							case "89":
								MessageBox.show("Email sent.");
								oRouter.navTo("UserReset");
								break;
							default:
								MessageBox.show("Valid process");
						}
					}
				};
				http.open("POST", url, false);
				http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				http.send(params);
			} else {
				MessageBox.show("Please enter a valid email address.");
			}
		},
		onLoginTap: function() { // returning back to login page
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("loginPage");
		}
	});

});

// inside of the return Controller.extend("cafeteria.controller.resetPassword", {

/**
 * Called when a controller is instantiated and its View controls (if available) are already created.
 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
 * @memberOf cafeteria.view.resetPassword
 */
//	onInit: function() {
//
//	},

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
 * (NOT before the first rendering! onInit() is used for that one!).
 * @memberOf cafeteria.view.resetPassword
 */
//	onBeforeRendering: function() {
//
//	},

/**
 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
 * This hook is the same one that SAPUI5 controls get after being rendered.
 * @memberOf cafeteria.view.resetPassword
 */
//	onAfterRendering: function() {
//
//	},

/**
 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
 * @memberOf cafeteria.view.resetPassword
 */
//	onExit: function() {
//
//	}