sap.ui.define([
	"sap/ui/core/mvc/Controller", "sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("cafeteria.controller.todayDownload", {
		onInit: function() {
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth() + 1; //January is 0!
			var yyyy = today.getFullYear();
			if (dd < 10) {
				dd = 0 + dd;
			}
			if (mm < 10) {
				mm = 0 + mm;
			}

			today = dd + "/" + mm + "/" + yyyy;
			MessageToast.show(today.toString(), {
				diplay: 100000
			});
			var oTextArea = new sap.ui.commons.TextArea({
				editable: false,
				value: today.toString(),
				wrapping: sap.ui.core.Wrapping.Soft
			}).placeAt("content");
			oTextArea.addStyleClass("text_area");
			// oTextArea.setEditable(false);
			// oTextArea.setValue(myText);
			// oTextArea.setWrapping(sap.ui.core.Wrapping.Soft);
		},
		onCancelTap: function() {
			sap.m.MessageBox.show("You will not proceed unless the downloading of today's data is done.", {
				icon: sap.m.MessageBox.Icon.INFORMATION,
				title: "You canceled the data downloading.",
				actions: [sap.m.MessageBox.Action.CLOSE],
				onClose: function(oAction) {
					var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
					oRouter.navTo("loginPage");
				}.bind(this, null)
			});

		}
	});
});
/**
 * Called when a controller is instantiated and its View controls (if available) are already created.
 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
 * @memberOf cafeteria.view.todayDownload
 */

/**
 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
 * (NOT before the first rendering! onInit() is used for that one!).
 * @memberOf cafeteria.view.todayDownload
 */
//	onBeforeRendering: function() {
//
//	},

/**
 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
 * This hook is the same one that SAPUI5 controls get after being rendered.
 * @memberOf cafeteria.view.todayDownload
 */
//	onAfterRendering: function() {
//
//	},

/**
 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
 * @memberOf cafeteria.view.todayDownload
 */
//	onExit: function() {
//
//	}