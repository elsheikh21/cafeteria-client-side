/* global XLSX:true */
sap.ui.define(["jquery.sap.global", "sap/m/MessageToast", "sap/m/MessageBox", "sap/ui/core/mvc/Controller"],
	function(jQuery, MessageToast, MessageBox, Controller) {
		"use strict";
		return Controller.extend("cafeteria.controller.upload", {
			handleUploadComplete: function(oEvent) {
				var sResponse = oEvent.getParameter("response");
				if (sResponse) {
					var sMsg = "";
					var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
					if (m !== null) {
						if (m[1] === "200") {
							sMsg = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Success)";
							oEvent.getSource().setValue("");
						} else {
							sMsg = "Return Code: " + m[1] + "\n" + m[2] + "(Upload Error)";
						}
						MessageToast.show("Uploaded? " + sMsg);
					} else {
						MessageToast.show("Unexpected error occured.");
					}
				}
			},
			handleUploadPress: function(oEvent) {
				var oFileUploader = this.getView().byId("fileUploader");
				if (!oFileUploader.getValue().toString()) {
					MessageToast.show("Choose a xlsx file first");
					return;
				}
				var temp = "";
				var url = "resources/test.xlsx";
				var oReq = new XMLHttpRequest();
				oReq.open("GET", url, true);
				oReq.responseType = "arraybuffer";
				oReq.onload = function(e) {
					var arraybuffer = oReq.response;
					var data = new Uint8Array(arraybuffer);
					var arr = [];
					for (var i = 0; i !== data.length; ++i) {
						arr[i] = String.fromCharCode(data[i]);
					}
					var bstr = arr.join("");
					var workbook = XLSX.read(bstr, {
						type: "binary"
					});
					var firstSheetName = workbook.SheetNames[0];
					var worksheet = workbook.Sheets[firstSheetName];
					var json = XLSX.utils.sheet_to_json(worksheet, {
						raw: true
					});
					var jsonStr = JSON.stringify(json);
					// MessageBox.show("JSON String: " + jsonStr);

					temp += jsonStr;
				};
				oReq.send();
				oFileUploader.upload();
			},
			handleTypeMissmatch: function(oEvent) {
				var aFileTypes = oEvent.getSource().getFileType();
				jQuery.each(aFileTypes, function(key, value) {
					aFileTypes[key] = "*." + value;
				});
				var sSupportedFileTypes = aFileTypes.join(", ");
				MessageToast.show("The file type *." + oEvent.getParameter("fileType") +
					" is not supported. Choose one of the following types: " +
					sSupportedFileTypes);
			},
			handleValueChange: function(oEvent) {
				MessageToast.show("Press 'Upload File' to upload file '" + oEvent.getParameter("newValue") + "'");
			}
		});
	});

// --------------------------------------------------------------------------------------------------------------------------------------------------------

// // var params = "json=";
// var oResourceBundle = this.getView().getModel("i18n").getResourceBundle();
// var url = oResourceBundle.getText("UploadExcelFile").toString();
// var oReq = new XMLHttpRequest();
// oReq.responseType = "arraybuffer";
// oReq.onload = function(e) {
// 	var arraybuffer = oReq.response;
// 	var data = new Uint8Array(arraybuffer);
// 	var arr = [];
// 	for (var i = 0; i !== data.length; ++i) {
// 		arr[i] = String.fromCharCode(data[i]);
// 	}
// 	var bstr = arr.join("");
// 	var workbook = XLSX.read(bstr, {
// 		type: "binary"
// 	});
// 	var FirstSheetName = workbook.SheetNames[0];
// 	var worksheet = workbook.Sheets[FirstSheetName];
// 	var json = JSON.stringify(XLSX.utils.sheet_to_json(worksheet, {
// 		header: 1
// 	}));
// 	MessageBox.show(json);
// };
// oReq.open("POST", url, true);
// oReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
// oReq.send();